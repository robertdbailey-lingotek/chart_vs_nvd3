(function() {

var FrequencyService = ['$http', function($http) {

  var item_reference = {
    '1': 'Never heard of it',
    '2': 'Heard of it, but haven\'t touched it',
    '3': 'Have played less than 1 hour',
    '4': 'Actively play, but not today',
    '5': 'Played it today',
    '6': 'Catching pokies right now'
  }

  return {
    list: function() {
      return $http.get('/api/frequency');
    },
    listRef: function() {
      return item_reference;
    },
    add: function(item_id) {
      return $http.post('/api/frequency', {'id': item_id});
    }
  };
}];

var TeamService = ['$http', function($http) {
  var items = {
    'blue': 0,
    'yellow': 0,
    'red': 0,
    'gray': 0
  };
  return {
    list: function() {
      $http.get('/api/team')
      .then(function success(response) {
        return response.data;
      });
    },
    add: function(item_id) {
      $http.post('/api/team', {'id': item_id})
      .then(function success(response) {
        return true;
      });
    }
  };
}];

var PowerService = ['$http', function($http) {
  var items = [];
  return {
    list: function() {
      $http.get('/api/power')
      .then(function success(response) {
        return response.data;
      });
    },
    add: function(item) {
      $http.post('api/power', {'power': item})
      .then(function success(response) {
        return true;
      });
    }
  };
}];

var QuestionnaireCtrl = ['$location', 'FrequencyService', 'TeamService', 'PowerService', function($location, FrequencyService, TeamService, PowerService) {
  var self = this;
  var frequency_response, team_response, power_response;
  self.frequency = [
    {'label': 'Never heard of it', 'value': '1'},
    {'label': 'Heard of it, but haven\'t touched it', 'value': '2'},
    {'label': 'Have played less than 1 hour', 'value': '3'},
    {'label': 'Actively play, but not today', 'value': '4'},
    {'label': 'Played it today', 'value': '5'},
    {'label': 'Catching pokies right now', 'value': '6'}
  ]
  self.teams = [
    {'label': 'Mystic (blue)', 'value': 'blue'},
    {'label': 'Instinct (yellow)', 'value': 'yellow'},
    {'label': 'Valor (red)', 'value': 'red'},
    {'label': 'No affiliation', 'value': 'gray'}
  ]
  self.submit = function() {
    FrequencyService.add(this.frequency_response);
    TeamService.add(this.team_response);
    PowerService.add(this.power_response);
    $location.path('/angular_chart')
  };
}];

var AngularChartCtrl = ['FrequencyService', 'TeamService', 'PowerService', function(FrequencyService, TeamService, PowerService) {
  var self = this;
  var frequency_reference = FrequencyService.listRef();
  var frequency_keys = Object.keys(frequency_reference);
  var frequency_vals = frequency_keys.map(function(v) { return frequency_reference[v]; });

  self.frequency_data = [0, 1, 2, 3, 4, 5];
  self.teams_data = {};
  self.power_data = [];

  self.bar_chart = {
    labels: frequency_vals,
    series: ['Attendees'],
    data: self.frequency_data,
    options: {
      scales: {
        yAxes: [
          {
            id: 'y-axis-1',
            type: 'linear',
            display: true,
            position: 'left'
          },
        ]
      }
    }
  };

  FrequencyService.list()
  .then(function (response) {
    self.frequency_data = frequency_keys.map(function(v) { return response.data[v]; });
  }, function (error) {
    console.log('unable to load frequency data: ' + error.message);
  })

  self.donut_chart = {
    labels: [
      "Not Affiliated",
      "Mystic (blue)",
      "Instinct (yellow)",
      "Valor (red)"
    ],
    data: [
      [5, 7, 2, 10],
    ],
    options: {
      legend: {
        display: true,
        position: 'left'
      },
      chartColors: ['grey', 'blue', 'yellow', 'red']
    },
    colors: ['grey', 'blue', 'yellow', 'red'],
  };

  self.line_chart = {
    series: ['CP'],
    labels: ['0', '200', '400', '600', '800', '1000', '1200', '1400', '1600'],
    data: [
      [876, 123, 965, 253, 325, 480, 1280, 342, 132, 175, 250, 98, 1500, 234, 506, 657, 235, 53, 908, 789],
    ],
  };
}];



var AngularNvd3Ctrl = function() {
  var self = this;
  ///////////////////////////////////////////////////////////////////////////////////
  // SAMPLES FROM ANGULAR-NVD3 HOMEPAGE (http://krispo.github.io/angular-nvd3/#/)
  ///////////////////////////////////////////////////////////////////////////////////
  
  /*Random Data Generator */
  var sinAndCos = function() {
      var sin = [],sin2 = [],
          cos = [];

      //Data is represented as an array of {x,y} pairs.
      for (var i = 0; i < 100; i++) {
          sin.push({x: i, y: Math.sin(i/10)});
          sin2.push({x: i, y: i % 10 == 5 ? null : Math.sin(i/10) *0.25 + 0.5});
          cos.push({x: i, y: .5 * Math.cos(i/10+ 2) + Math.random() / 10});
      }

      //Line chart data should be sent as an array of series objects.
      return [
          {
              values: sin,      //values - represents the array of {x,y} data points
              key: 'Sine Wave', //key  - the name of the series.
              color: '#ff7f0e'  //color - optional: choose your own line color.
          },
          {
              values: cos,
              key: 'Cosine Wave',
              color: '#2ca02c'
          },
          {
              values: sin2,
              key: 'Another sine wave',
              color: '#7777ff',
              area: true      //area - set to true if you want this line to turn into a filled area chart.
          }
      ];
  };

  this.bar_chart = {
    options: {
      chart: {
        type: 'historicalBarChart',
        height: 450,
        margin : {
            top: 20,
            right: 20,
            bottom: 65,
            left: 50
        },
        x: function(d){return d[0];},
        y: function(d){return d[1]/100000;},
        showValues: true,
        valueFormat: function(d){
            return d3.format(',.1f')(d);
        },
        duration: 100,
        xAxis: {
            axisLabel: 'X Axis',
            tickFormat: function(d) {
                return d3.time.format('%x')(new Date(d))
            },
            rotateLabels: 30,
            showMaxMin: false
        },
        yAxis: {
            axisLabel: 'Y Axis',
            axisLabelDistance: -10,
            tickFormat: function(d){
                return d3.format(',.1f')(d);
            }
        },
        tooltip: {
            keyFormatter: function(d) {
                return d3.time.format('%x')(new Date(d));
            }
        },
        zoom: {
            enabled: true,
            scaleExtent: [1, 10],
            useFixedDomain: false,
            useNiceScale: false,
            horizontalOff: false,
            verticalOff: true,
            unzoomEventType: 'dblclick.zoom'
        }
      }
    },
    data: [
      {
        "key" : "Quantity" ,
        "bar": true,
        "values" : [
          [ 1136005200000 , 1271000.0] ,
           [ 1138683600000 , 1271000.0] ,
           [ 1141102800000 , 1271000.0] ,
           [ 1143781200000 , 0] ,
           [ 1146369600000 , 0] ,
           [ 1149048000000 , 0] ,
           [ 1151640000000 , 0] ,
           [ 1154318400000 , 0] ,
           [ 1156996800000 , 0] ,
           [ 1159588800000 , 3899486.0] ,
           [ 1162270800000 , 3899486.0] ,
           [ 1164862800000 , 3899486.0] ,
           [ 1167541200000 , 3564700.0] ,
           [ 1170219600000 , 3564700.0] ,
           [ 1172638800000 , 3564700.0] ,
           [ 1175313600000 , 2648493.0] ,
           [ 1177905600000 , 2648493.0] ,
           [ 1180584000000 , 2648493.0] ,
           [ 1183176000000 , 2522993.0] ,
           [ 1185854400000 , 2522993.0] ,
           [ 1188532800000 , 2522993.0] ,
           [ 1191124800000 , 2906501.0] ,
           [ 1193803200000 , 2906501.0] ,
           [ 1196398800000 , 2906501.0] ,
           [ 1199077200000 , 2206761.0] ,
           [ 1201755600000 , 2206761.0] ,
           [ 1204261200000 , 2206761.0] ,
           [ 1206936000000 , 2287726.0] ,
           [ 1209528000000 , 2287726.0] ,
           [ 1212206400000 , 2287726.0] ,
           [ 1214798400000 , 2732646.0] ,
           [ 1217476800000 , 2732646.0] ,
           [ 1220155200000 , 2732646.0] ,
           [ 1222747200000 , 2599196.0] ,
           [ 1225425600000 , 2599196.0] ,
           [ 1228021200000 , 2599196.0] ,
           [ 1230699600000 , 1924387.0] ,
           [ 1233378000000 , 1924387.0] ,
           [ 1235797200000 , 1924387.0] ,
           [ 1238472000000 , 1756311.0] ,
           [ 1241064000000 , 1756311.0] ,
           [ 1243742400000 , 1756311.0] ,
           [ 1246334400000 , 1743470.0] ,
           [ 1249012800000 , 1743470.0] ,
           [ 1251691200000 , 1743470.0] ,
           [ 1254283200000 , 1519010.0] ,
           [ 1256961600000 , 1519010.0] ,
           [ 1259557200000 , 1519010.0] ,
           [ 1262235600000 , 1591444.0] ,
           [ 1264914000000 , 1591444.0] ,
           [ 1267333200000 , 1591444.0] ,
           [ 1270008000000 , 1543784.0] ,
           [ 1272600000000 , 1543784.0] ,
           [ 1275278400000 , 1543784.0] ,
           [ 1277870400000 , 1309915.0] ,
           [ 1280548800000 , 1309915.0] ,
           [ 1283227200000 , 1309915.0] ,
           [ 1285819200000 , 1331875.0] ,
           [ 1288497600000 , 1331875.0] ,
           [ 1291093200000 , 1331875.0] ,
           [ 1293771600000 , 1331875.0] ,
           [ 1296450000000 , 1154695.0] ,
           [ 1298869200000 , 1154695.0] ,
           [ 1301544000000 , 1194025.0] ,
           [ 1304136000000 , 1194025.0] ,
           [ 1306814400000 , 1194025.0] ,
           [ 1309406400000 , 1194025.0] ,
           [ 1312084800000 , 1194025.0] ,
           [ 1314763200000 , 1244525.0] ,
           [ 1317355200000 , 475000.0] ,
           [ 1320033600000 , 475000.0] ,
           [ 1322629200000 , 475000.0] ,
           [ 1325307600000 , 690033.0] ,
           [ 1327986000000 , 690033.0] ,
           [ 1330491600000 , 690033.0] ,
           [ 1333166400000 , 514733.0] ,
           [ 1335758400000 , 514733.0]]
      }
    ]
  };

  this.donut_chart = {
    options: {
                chart: {
                    type: 'pieChart',
                    height: 450,
                    donut: true,
                    x: function(d){return d.key;},
                    y: function(d){return d.y;},
                    showLabels: true,

                    pie: {
                        startAngle: function(d) { return d.startAngle/2 -Math.PI/2 },
                        endAngle: function(d) { return d.endAngle/2 -Math.PI/2 }
                    },
                    duration: 500,
                    legend: {
                        margin: {
                            top: 5,
                            right: 140,
                            bottom: 5,
                            left: 0
                        }
                    }
                }
            },

    data: [
                { key: "One", y: 5 },
                { key: "Two", y: 2 },
                { key: "Three", y: 9 },
                { key: "Four", y: 7 },
                { key: "Five", y: 4 },
                { key: "Six", y: 3 },
                { key: "Seven", y: .5 }
            ]
  };

  this.line_chart = {
    options: {
                chart: {
                    type: 'lineChart',
                    height: 450,
                    margin : {
                        top: 20,
                        right: 20,
                        bottom: 40,
                        left: 55
                    },
                    x: function(d){ return d.x; },
                    y: function(d){ return d.y; },
                    useInteractiveGuideline: true,
                    dispatch: {
                        stateChange: function(e){ console.log("stateChange"); },
                        changeState: function(e){ console.log("changeState"); },
                        tooltipShow: function(e){ console.log("tooltipShow"); },
                        tooltipHide: function(e){ console.log("tooltipHide"); }
                    },
                    xAxis: {
                        axisLabel: 'Time (ms)'
                    },
                    yAxis: {
                        axisLabel: 'Voltage (v)',
                        tickFormat: function(d){
                            return d3.format('.02f')(d);
                        },
                        axisLabelDistance: -10
                    },
                    callback: function(chart){
                        console.log("!!! lineChart callback !!!");
                    }
                },
            },

            data: sinAndCos()
  };
};

angular.module("app", ["ngMaterial", "ngMessages", "ui.router", "chart.js", "nvd3"])
  .config([
    'ChartJsProvider', function(ChartJsProvider) {
      return ChartJsProvider.setOptions({
        legend: {
          display: true,
          position: 'right'
        },
        chartColors: ['gray', 'red', 'yellow', 'blue', 'brown', 'green', 'black']
      });
    }
  ])
  .config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/questionnaire");
    //
    // Now set up the states
    $stateProvider
      .state('questionnaire', {
        url: "/questionnaire",
        templateUrl: "partials/questionnaire.html",
        controller: QuestionnaireCtrl
      })
      .state('angular_chart', {
        url: "/angular_chart",
        templateUrl: "partials/angular_chart.html",
        controller: AngularChartCtrl
      })
      .state('angular_nvd3', {
        url: "/angular_nvd3",
          templateUrl: "partials/angular_nvd3.html",
          controller: function($scope) {
            $scope.things = ["A", "Set", "Of", "Things"];
          }
      })
    }])
  .controller("QuestionnaireCtrl", QuestionnaireCtrl)
  .controller("AngularChartCtrl", AngularChartCtrl)
  .controller("AngularNvd3Ctrl", AngularNvd3Ctrl)
  .factory('FrequencyService', FrequencyService)
  .factory('TeamService', TeamService)
  .factory('PowerService', PowerService);

})();

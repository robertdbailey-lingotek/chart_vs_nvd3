var express = require('express');
var router = express.Router();
var server = express();
var body_parser = require('body-parser');
var storage = require('node-persist');

server.use(body_parser.json());
server.use(body_parser.urlencoded({ extended: true }));

// INITIALIZE THE STORAGE
frequency = {
  '1': 0, // never heard of it
  '2': 0, // heard of it, haven't touched it
  '3': 0, // have played < 1 hour
  '4': 0, // actively play, but not today
  '5': 0, // played it today
  '6': 0  // catching pokies now
};
var teams = {
    'blue': 0,
    'yellow': 0,
    'red': 0,
    'gray': 0
};
var power = [];
storage.initSync();
storage.getItem('frequency', function (err, value) {
  if (!value) {
    storage.setItem('frequency', frequency);
  }
});
storage.getItem('teams', function (err, value) {
  if (!value) {
    storage.setItem('teams', teams);
  }
});
storage.getItem('power', function (err, value) {
  if (!value) {
    storage.setItem('power', power);
  }
});

// ADD THE STAT-RELATED REST ROUTES
server.get('/api/frequency', function (req, res) {
  res.send(storage.getItem('frequency'));
});
server.post('/api/frequency', function (req, res) {
  var frequency = storage.getItem('frequency');
  frequency[req.body.id]++
  storage.setItem('frequency', frequency);
  res.sendStatus(200);
});
server.get('/api/team', function (req, res) {
  res.send(storage.getItem('teams'));
});
server.post('/api/team', function (req, res) {
  var teams = storage.getItem('teams');
  teams[req.body.id]++
  storage.setItem('teams', teams);
  res.sendStatus(200);
});
server.get('/api/power', function (req, res) {
  res.send(storage.getItem('power'));
});
server.post('/api/power', function (req, res) {
  debugger;
  if (req.body.power) {
    var power = storage.getItem('power');
    power.push(req.body.power);
    storage.setItem('power', power);
  }
  res.sendStatus(200);
});

// SERVE IT UP!
router.use(express.static('.'));
server.use(router);
server.listen(3000, function () {
  console.log('server listening on 3000');
});

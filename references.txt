https://www.youtube.com/watch?v=aqHBLS_6gF8

Angular Charts (based on angular and d3):
https://github.com/chinmaymk/angular-charts

Angular Chart (based on angular and chart.js):
https://jtblin.github.io/angular-chart.js/

Other charting tools based on d3: nvd3.js, dimple.js, dc.js

For angular-nvd3:
bower install angular-nvd3 (installs angular, d3, and nvd3 as dependencies)

For angular chart:
npm install angular-chart.js --save
